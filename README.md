## Setup
```bash
virtualenv -p python3.6 venv
```
```bash
source venv/bin/activate
```
```bash
pip install -r requirements.txt
```
```bash
python manage.py migrate
```
## Run server
```bash
python manage.py runserver
```
## Run Bot
```bash
python bot.py
```
## Postman collection
[![Run in Postman](https://run.pstmn.io/button.svg)](https://www.getpostman.com/collections/b9bbedec6f9262fb9f7c)
