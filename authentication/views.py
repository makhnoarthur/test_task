from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from authentication.models import LastActivity
from authentication.serializers import (UserDetailSerializer,
                                        UserSignInSerializer,
                                        UserSignUpSerializer)


class SignUpView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        serializer = UserSignUpSerializer(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        user = serializer.save()
        return Response(status=status.HTTP_201_CREATED,
                        data={'user': UserDetailSerializer(user).data})


class SignInView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request):
        serializer = UserSignInSerializer(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        user = serializer.save()
        return Response(status=status.HTTP_200_OK,
                        data={'user': UserDetailSerializer(user).data})


class UserActivityView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        last_activity = LastActivity.objects.filter(user=request.user).first()
        data = {
            'last_login': request.user.last_login,
            'last_activity_time': last_activity.date if last_activity else None
        }

        return Response(status=status.HTTP_200_OK,
                        data=data)
