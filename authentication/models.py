import hashlib

from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from post.models import Like, Post


class User(AbstractBaseUser):
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True, unique=True)
    last_login = models.DateTimeField(auto_now=True)
    password = models.CharField(max_length=255, blank=True, null=True)

    USERNAME_FIELD = 'email'

    class Meta:
        db_table = 'user'

    def set_password(self, password):
        self.password = self.encode(password)

    def check_password(self, password):
        return self.password == self.encode(password)

    def encode(self, password):
        return hashlib.sha1(password.encode()).hexdigest()

    def generate_auth_token(self):
        if hasattr(self, 'auth_token'):
            self.auth_token.delete()
        Token.objects.create(user=self)


class LastActivity(models.Model):
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey('authentication.User', on_delete=models.CASCADE)

    class Meta:
        db_table = 'last_activity'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    LastActivity.objects.update_or_create(user_id=instance.id)


@receiver(post_save, sender=Post)
def create_post(sender, instance, created, **kwargs):
    LastActivity.objects.update_or_create(user_id=instance.user_id)


@receiver(post_save, sender=Like)
def liked_post(sender, instance, created, **kwargs):
    LastActivity.objects.update_or_create(user_id=instance.user_id)
