from django.urls import path

from authentication.views import SignInView, SignUpView, UserActivityView

urlpatterns = [
    path('signup/', SignUpView.as_view()),
    path('signin/', SignInView.as_view()),
    path('user-activity/', UserActivityView.as_view()),
]
