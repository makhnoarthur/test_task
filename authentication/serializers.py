from rest_framework import serializers

from authentication.models import User


class UserDetailSerializer(serializers.ModelSerializer):
    auth_token = serializers.CharField(source='auth_token.key')

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'auth_token')


class UserSignInSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)
    password = serializers.CharField(write_only=True)

    def validate(self, attrs):
        try:
            user = User.objects.get(email=attrs['email'])
        except User.DoesNotExist:
            user = None
        if user is None or not user.check_password(attrs['password']):
            raise serializers.ValidationError('Incorrect credentials!')
        attrs['user'] = user
        return attrs

    def create(self, validated_data):
        user = validated_data['user']
        user.generate_auth_token()
        user.save(update_fields=['last_login'])
        return user

    def get_user(self):
        try:
            user = User.objects.get(email=self.validated_data['email'])
        except User.DoesNotExist:
            user = None
        return user


class UserSignUpSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    email = serializers.EmailField()
    password = serializers.RegexField(
        regex=r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$')

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.\
                ValidationError('User with this email already exists!')
        return value

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(
            **validated_data
        )
        user.set_password(password)
        user.save()
        user.generate_auth_token()
        return user
