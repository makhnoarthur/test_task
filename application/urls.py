from django.urls import include, path

urlpatterns = [
    path('authentication/', include('authentication.urls')),
    path('post/', include('post.urls')),
]
