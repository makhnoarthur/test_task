import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.dev')
django.setup()

from authentication.models import User
from post.models import Post
import random
from django.utils.crypto import get_random_string


##### FILE #####

number_of_users = 10
max_posts_per_user = 14
max_likes_per_user = 11

################

FIRST_NAMES = ['a', 'b', 'c', 'd', 'e']
LAST_NAMES = ['a', 'b', 'c', 'd', 'e']
EMAIL = ['gmail.com', 'mail.ru']

User.objects.all().delete()
Post.objects.all().delete()


def create_users():
    users = []
    for _ in range(number_of_users):
        user = User(first_name=random.choice(FIRST_NAMES), last_name=random.choice(LAST_NAMES),
                    email=f'{get_random_string(length=8)}@{random.choice(EMAIL)}')
        user.set_password('123456789Aa')
        user.save()
        users.append(user)
    return users


def create_user_post(user):
    posts = []
    for _ in range(max_posts_per_user):
        post = Post(text='title', user_id=user.id)
        post.save()
        posts.append(post)
    return posts


def user_likes_posts(user):
    posts = Post.objects.order_by('?')[:random.randint(0, max_likes_per_user)]
    for post in posts:
        post.likes_of_user(user)


def main():
    users = create_users()
    for user in users:
        create_user_post(user)
    for user in users:
        user_likes_posts(user)


if __name__ == '__main__':
    main()
