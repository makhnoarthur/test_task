import django_filters

from post.models import Like


class LikesAnalyticsFilter(django_filters.FilterSet):
    created_at = django_filters.DateFromToRangeFilter()

    class Meta:
        model = Like
        fields = ('created_at', )
