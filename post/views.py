from django.db.models import Count
from django.db.models.functions import TruncDay
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from post.filters import LikesAnalyticsFilter
from post.models import Like, Post
from post.serializers import PostSerializer


class PostCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request):
        request.data['user'] = request.user.id

        serializer = PostSerializer(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response({'status': 0}, status=status.HTTP_400_BAD_REQUEST)
        post = serializer.save()
        return Response({'status': 1,
                         'post': PostSerializer(post).data})


class LikeView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, post_id):

        post = get_object_or_404(Post, id=post_id)
        reaction = post.likes_of_user(request.user)

        return Response(status=status.HTTP_200_OK, data={'reaction': reaction})


class LikesAnalyticsView(APIView):

    permission_classes = (IsAuthenticated, )

    filterset_class = LikesAnalyticsFilter

    def get(self, request, post_id):
        reaction_filter = LikesAnalyticsFilter(request.GET)

        if not reaction_filter.is_valid():
            return Response({'status': 0}, status=status.HTTP_400_BAD_REQUEST)

        likes = Like.objects.filter(post_id=post_id).all()
        filtered_queryset = reaction_filter.filter_queryset(likes)

        likes_count = filtered_queryset.annotate(day=TruncDay('created_at'))\
            .values('day').annotate(likes=Count('id'))

        return Response(status=status.HTTP_200_OK, data=likes_count)
