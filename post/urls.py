from django.urls import path

from post.views import LikesAnalyticsView, LikeView, PostCreateView

urlpatterns = [
    path('', PostCreateView.as_view()),
    path('like/<int:post_id>/', LikeView.as_view()),
    path('analytics/<int:post_id>/', LikesAnalyticsView.as_view()),
]
