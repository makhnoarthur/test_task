from django.db import models


class Post(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('authentication.User', on_delete=models.CASCADE)
    text = models.TextField()

    class Meta:
        db_table = 'post'

    def likes_of_user(self, user):
        if Like.objects.filter(user_id=user.id, post=self.id):
            Like.objects.filter(user_id=user.id, post=self.id).delete()
            return "unlike"
        else:
            Like.objects.create(user_id=user.id, post_id=self.id)
            return "like"


class Like(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('authentication.User', on_delete=models.CASCADE)
    post = models.ForeignKey('post.Post', on_delete=models.CASCADE)

    class Meta:
        db_table = 'like'
